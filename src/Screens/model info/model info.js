import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './model info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {  message,Modal, Form, Input,Icon} from 'antd';
import {Table} from 'react-materialize'

class ModelInfo extends React.Component {
    state = {
        modal2Visible: false,
         model:this.props.location.state.data,
        
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        


     deleteModel = () => {
      let l = message.loading(allStrings.wait, 2.5)
      axios.delete(`${BASE_END_POINT}model/${this.state.model.id}`,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
      .then(response=>{
          l.then(() => message.success(allStrings.deleteDone, 2.5))
          this.props.history.goBack()
      })
      .catch(error=>{
          console.log(error.response)
          l.then(() => message.error('Error', 2.5))
      })
   }

    
    handleSubmitAdd = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll(['modelname'],(err, values) => {
        if (!err) {
          //console.log('Received values of form: ', values);
         var data ={
          modelname: values.modelname,
          brand: this.state.model.brand.id
         }
          
          let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}model/${this.state.model.id}`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }


      setModal2Visible(modal2Visible) {
        this.setState({ modal2Visible });
      }
    render() {
      console.log(this.state.model)
        const { getFieldDecorator } = this.props.form;
         //select
        
         const {model} = this.state  
        const {select} = this.props;

      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff',marginTop:"10px"}}>{allStrings.ModelInfo}</h2>
                </div>
               
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s12">
                            <input id="name" type="text" class="validate" disabled value={model.id}>
                            </input>
                            <label for="name" class="active">{allStrings.id}</label>
                            </div>
                            <div class="input-field col s12">
                            <input id="name" type="text" class="validate" disabled value={model.modelname}>
                            </input>
                            <label for="name" class="active">{allStrings.modelname}</label>
                            </div>

                        </div>

                       
                        
                       
                            <a className="waves-effect waves-light btn btn-large delete " onClick={this.deleteModel}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a className="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal2Visible(true)}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>
                       

                            
                            <div>
                            <Modal
                                title={allStrings.addModel}
                                visible={this.state.modal2Visible}
                                onOk={this.handleSubmitAdd}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal2Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmitAdd} className="login-form">

                                  <Form.Item>
                                  {getFieldDecorator('modelname', {
                                      rules: [{ required: true, message: 'Please enter model name' }],
                                      initialValue: model.modelname
                                  })(
                                      <Input placeholder={allStrings.modelname}   />
                                  )}
                                  </Form.Item>

                           
                                
                                 
                              </Form>
                            </Modal>

                                
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(ModelInfo = Form.create({ name: 'normal_login', })(ModelInfo)) ;
