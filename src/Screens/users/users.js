
import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import './users.css';
//import {Icon,Button,Modal,Input} from 'react-materialize';
import {Skeleton,Modal, Form,Select, Icon, Input, Button,  Popconfirm, message} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
//

class User extends React.Component {
 
    constructor(props){
      super(props)
      this.getUsers(1)
      this.getCountries()
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }
   
    pagentationPage=0;
    counter=0;
    state = {
        modal1Visible: false,
        confirmDelete: false,
        selectedUser:null,
        users:[],
        loading:true,
        countries:[],
        tablePage:0,
        }

        getCountries = () => {
          axios.get(`${BASE_END_POINT}countries`)
          .then(response=>{
            this.setState({countries:response.data.data})
          })
          .catch(error=>{
            //console.log("ALL Categories ERROR")
            //console.log(error.response)
          })
        }

      flag = 0;
       getUsers = (page,deleteRow) => {
         axios.get(`${BASE_END_POINT}getAll?type=CLIENT&page=${page}&limit={20}`)
         .then(response=>{
           console.log("ALL users")
           console.log(response.data)
           if(deleteRow){
            this.setState({tablePage:0})
           }
           this.setState({users:deleteRow?response.data.data:[...this.state.users,...response.data.data],loading:false})
         })
         .catch(error=>{
          // console.log("ALL users ERROR")
           //console.log(error.response)
         })
       }
     
       OKBUTTON = (e) => {
        this.deleteUser()
       }
 
       deleteUser = () => {
         let l = message.loading(allStrings.wait, 2.5)
         axios.delete(`${BASE_END_POINT}${this.state.selectedUser}/delete`,{
           headers: {
             'Content-Type': 'application/x-www-form-urlencoded',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
         .then(response=>{
             l.then(() => message.success(allStrings.deleteDone, 2.5))
             this.getUsers(1,true)
             this.flag = 0
         })
         .catch(error=>{
             //console.log(error.response)
             l.then(() => message.error('Error', 2.5))
         })
      }

    //submit form
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            //console.log('date ', date[2]  );
            const data = {
                username: values.username,
                phone: values.phone,
                email: values.email,
                country: values.country.key,
                password: values.password,
                type: 'CLIENT',
                                                           
            }
            
            let l = message.loading(allStrings.wait, 2.5)
            axios.post(`${BASE_END_POINT}signup`,JSON.stringify(data),{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
              console.log(allStrings.addDone)
                l.then(() => message.success('Add User', 2.5));
                this.setState({ modal1Visible:false });
                this.getUsers(1,true)
                this.flag = 0
                this.props.form.resetFields()
            })
            .catch(error=>{
              console.log("Add USer Error")
                console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })

          }
        });
        
      }
    //end submit form
      


    showModal = () => {
      this.setState({
        visible: true,
      });
      
    }
    

    setModal1Visible(modal1Visible) {
      this.setState({ modal1Visible });
    }


    setModal2Visible(modal2Visible) {
      this.setState({ modal2Visible });
    }

    validatePhone = (rule, value, callback) => {
      //const { form } = this.props;
      if ( isNaN(value) ) {
        callback('Please enter coorect phone');
      }else if ( value.length <11 ) {
        callback('Phone number must be grater than 11 digit');
      } 
      else {
        callback();
      }
    };
  
  
//end modal//

//end modal

    render() {
      const Option = Select.Option;
      const{select} = this.props
        //form
         const { getFieldDecorator } = this.props.form;
        
         //end select
         let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.yes}
            cancelText={allStrings.no}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
        
        
         let list = this.state.users.map(val=>[""+val.id,""+val.username,""+val.phone,""+val.email,
         ""+val.country,val.block?allStrings.yes:allStrings.no,controls
        ])
         const loadingView = [
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          
         ]
      return (
          <div>
               <AppMenu  height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
              <Tables 
              columns={this.state.loading?['Loading...']:[allStrings.id,allStrings.name, allStrings.phone,allStrings.email,allStrings.country,allStrings.block,allStrings.remove]} title={allStrings.userstable}
              arr={this.state.loading?loadingView:list}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!==6){
                  console.log(colData)
                  console.log('row   ',cellMeta.rowIndex+"         "+cellMeta.dataIndex)
                  this.props.history.push('/UserInfo',{data:this.state.users[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===6){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedUser:id})
                    //console.log(id)
                  }
              }}

              
              onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{ 
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                console.log('current',currentPage)
              
                if(currentPage%2!=0  && currentPage > this.flag){
                  let rem = currentPage %2;
                  let x = currentPage - rem;
                  console.log(rem)
                  console.log(x)
                  this.flag  = this.flag +1;
                  console.log("flage",this.flag)
                    let page = rem + this.flag;
                    console.log("page",page)
                    this.getUsers(page)
                    console.log("request",page)
                    console.log("flag",this.flag) 

                } 
               

                  
              }}
              ></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:60}}  onClick={() => this.setModal1Visible(true)}>{allStrings.addUser}</Button>
              <Modal
                    title={allStrings.add}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                    onCancel={() => this.setModal1Visible(false)}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                    {getFieldDecorator('username', {
                        rules: [{ required: true, message: 'Please enter  Name' }],
                    })(
                        <Input placeholder={allStrings.name} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please enter  Password' }],
                    })(
                        <Input placeholder={allStrings.password} />
                    )}
                    </Form.Item>
                                                        
                    <Form.Item>
                    {getFieldDecorator('email', {
                        rules: [
                          { required: true, message: 'Please enter email' },
                          {type: 'email',  message: 'Please enter correct email' }
                        ],
                    })(
                        <Input placeholder={allStrings.email} />
                    )}
                    </Form.Item>

                    <Form.Item hasFeedback >
                    {getFieldDecorator('phone', {
                        rules: [
                          {required: true, message: 'Please enter phone' },
                          {
                            validator: this.validatePhone,
                          },
                        ],
                        
                    })(
                        <Input placeholder={allStrings.phone} />
                    )}
                    </Form.Item>
                  

                  

                    <Form.Item>
                    {getFieldDecorator('country', {
                        rules: [{ required: true, message: 'Please enter country' }],
                    })(
                      <Select labelInValue  
                      placeholder={allStrings.country}
                      style={{ width: '100%'}} >
                           {this.state.countries.map(val=>
                           <Option value={val.countryName}>{this.props.isRTL?val.arabicName:val.countryName}</Option>
                           )}                 
                      </Select>
                    )}
                    </Form.Item>

                   
                   
                   
                    </Form>
                </Modal>
              
              </div>
              </div>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }


  export default withRouter( connect(mapToStateProps,mapDispatchToProps)  (User = Form.create({ name: 'normal_login' })(User)) );
 

