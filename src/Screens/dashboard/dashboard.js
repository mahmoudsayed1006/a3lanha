import React, { Component } from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';

import './dashboard.css';
import {Icon,Table} from 'react-materialize'
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {NavLink} from 'react-router-dom';
import { notification,Skeleton,Checkbox,message} from 'antd';
import 'antd/dist/antd.css';
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {withRouter} from 'react-router-dom'


class Dashboard extends Component {
   
  state = {
    users:[],
    counts:[],
    actions:[],
    loading:true,
    loading2:true,
    checked:false,
   
}

constructor(props){
    super(props)
    if(this.props.isRTL){
      allStrings.setLanguage('ar')
    }else{
      allStrings.setLanguage('en')
    }

  }
  
 
    componentDidMount(){
       // console.log(this.props.currentUser)
        console.log("push   ",this.props.history)
        this.getCounts()
        this.getOption()
        this.getLastAction()
            this.getLastUsers()
    }
       
       //submit form
       //PENDING
       getCounts= () => {
         axios.get(`${BASE_END_POINT}admin/count`,{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
         .then(response=>{
           console.log("ALL counts")
           console.log(response.data)
           this.setState({counts:response.data,loading:false})
         })
         .catch(error=>{
           console.log("ALL counts ERROR")
           console.log(error.response)
         })
       }
       getLastUsers= () => {
        axios.get(`${BASE_END_POINT}admin/users`,{
           headers: {
             'Content-Type': 'application/json',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
        .then(response=>{
          //console.log("ALL last users")
          //console.log(response.data)
          this.setState({users:response.data,loading2:false})
        })
        .catch(error=>{
          //console.log("ALL last users ERROR")
          //console.log(error.response)
        })
      }
      getLastAction= () => {
        axios.get(`${BASE_END_POINT}admin/actions`,{
           headers: {
             'Content-Type': 'application/json',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
        .then(response=>{
          //console.log("ALL last actions")
          //console.log(response.data)
          this.setState({actions:response.data,loading2:false})
        })
        .catch(error=>{
          //console.log("ALL last orders ERROR")
         // console.log(error.response)
        })
      }
      getOption= () => {
        axios.get(`${BASE_END_POINT}option`,{
           headers: {
             'Content-Type': 'application/json',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
        .then(response=>{
          console.log(response.data[0].enable)
          this.setState({checked:response.data[0].enable,loading:false})
        })
        .catch(error=>{
          console.log("ALL counts ERROR")
          console.log(error.response)
        })
      }
      option = (check) => {
        let uri ='';
        if(check){
         uri = `${BASE_END_POINT}/option/1/enable`
        }else{
         uri = `${BASE_END_POINT}/option/1/disable`
        }
       let l = message.loading(allStrings.wait, 2.5)
        axios.put(uri,{},{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
        .then(response=>{
             console.log('done')
             if(check){
                 
                 l.then(() => message.success(allStrings.on, 2.5))
                 
             }else{
             
                l.then(() => message.success(allStrings.off, 2.5))
             }
        })
        .catch(error=>{
         console.log('Error')
         console.log(error.response)
         l.then(() => message.error('Error', 2.5))
        })
    }
        
    onChange = e => {
      console.log('checked = ', e.target.checked);
      this.setState({
        checked: e.target.checked,
      });
      if(e.target.checked === true){
        this.option(true)
      } else{
        this.option(false)
      }
    };
  render() {
      const {counts} = this.state;
      const {users} = this.state;
      const {actions} = this.state;
      const loadingView = 
       <Skeleton  active/> 

       const{select} = this.props
       //alert(''+window.innerHeight)
    return (
      <div >
        <AppMenu height={'200%'} goTo={this.props.history}></AppMenu>
        <Nav></Nav>
        
        <div style={{marginRight:!select?'20.2%':'5.5%'}}>
          <div className='content' >
          <div className="row" style={{marginLeft: '1%'}}>
          <Checkbox
            checked={this.state.checked}
            onChange={this.onChange}
          >
            {allStrings.option}
          </Checkbox>
            </div>
              <div className="row">
                  <div className="col s12 m6 xl4 l6 count1">
                  <NavLink to='/users'>
                  <div className="icons">
                      <Icon>group</Icon>
                  </div>
                  <div className='info' >
                      <p>{allStrings.user}</p>
                      <span>{this.state.loading?loadingView:counts.clients}</span>
                  </div>
                  </NavLink>
                  </div>          
                 

                  <div className="col s12 m6 xl4 l6 count2" >
                  <NavLink to='/Admins'>
                      <div className="icons">
                      <Icon>person_outline</Icon>
                      </div>
                      <div className='info'>
                          <p>{allStrings.admin}</p>
                          <span>{this.state.loading?loadingView:counts.admins}</span>
                      </div>
                  </NavLink>
                  </div>


                  <div className="col s12 m6 xl4 l6 count3">
                  <NavLink to='/category'>
                      <div className="icons">
                      <Icon>category</Icon> 
                      </div>
                      <div className='info'>
                          <p>{allStrings.subCategory}</p>
                          <span>{this.state.loading?loadingView:counts.subCategoryCount}</span>
                      </div>
                  </NavLink>
                  </div> 
                
              </div>
              
              <div className="row">
                  <div className="col s12 m6 xl4 l6 count1">
                  <NavLink to='/Main category'>
                  <div className="icons">
                  <Icon>widgets</Icon>
                  </div>
                  <div className='info'>
                      <p>{allStrings.mainCategories}</p>
                      <span>{this.state.loading?loadingView:""+counts.mainCategoryCount}</span>
                  </div>
                  </NavLink>
                  </div>
                 
                  <div className="col s12 m6 xl4 l6 count2">
                  <NavLink to='/Ads'>
                      <div className="icons">
                      <Icon>airplay</Icon> 
                      </div>
                      <div className='info'>
                          <p>{allStrings.ads}</p>
                          <span>{this.state.loading?loadingView:""+counts.adsCount}</span>
                      </div>
                  </NavLink>
                  </div>
                  
                  <div className="col s12 m6 xl4 l6 count3">
                  <NavLink to='/announcement'> 
                      <div className="icons">
                      <Icon>assistant</Icon>
                      </div>
                      <div className='info'>
                          <p>{allStrings.announcement}</p>
                          <span>{this.state.loading?loadingView:""+counts.anoncementCount}</span>
                      </div>
                  </NavLink>
                  </div> 
                                
              </div>


              <div className="row">
                  <div className="col s12 m6 xl4 l6 count1">
                  <NavLink to='/Contactus'>

                  <div className="icons">
                   <Icon>mail_outline</Icon>
                  </div>
                  <div className='info'>
                      <p>{allStrings.messages}</p>
                      <span>{this.state.loading?loadingView:""+counts.messages}</span>
                  </div>
                  </NavLink>
                  </div>
                  <div className="col s12 m6 xl4 l6 count2">
                  <NavLink to='/countries'>
                  <div className="icons">
                  <Icon>assistant_photo</Icon>
                  </div>
                  <div className='info'>
                      <p>{allStrings.countries}</p>
                      <span>{this.state.loading?loadingView:counts.countriesCount}</span>
                  </div>
                  </NavLink>
                  </div>
                 
                  <div className="col s12 m6 xl4 l6 count3">
                  <NavLink to='/brand'>
                      <div className="icons">
                      <Icon>directions_car</Icon> 
                      </div>
                      <div className='info'>
                          <p>{allStrings.brand}</p>
                          <span>{this.state.loading?loadingView:counts.brandCount}</span>
                      </div>
                      </NavLink>
                  </div> 
                                   
              </div>
            


          </div>
          </div>
      </div>
    );
  }
}

const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    
  }

export default withRouter( connect(mapToStateProps,mapDispatchToProps) (Dashboard) );
