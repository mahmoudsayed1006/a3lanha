
import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import './problem.css';
//import {Icon,Button,Modal,Input} from 'react-materialize';
import {Skeleton,Modal, Form,Select, Icon, Input, Button,  Popconfirm, message} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
//

class Problem extends React.Component {
 
    constructor(props){
      super(props)
      this.getProblems(1)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }
   
    pagentationPage=0;
    counter=0;
    state = {
        modal1Visible: false,
        confirmDelete: false,
        selectedProblem:null,
        problems:[],
        loading:true,
        tablePage:0,
        }

       
      flag = 0;
       getProblems = (page,deleteRow) => {
         axios.get(`${BASE_END_POINT}problem?page=${page}&limit={20}`)
         .then(response=>{
           console.log("ALL problems")
           console.log(response.data)
           if(deleteRow){
            this.setState({tablePage:0})
           }
           this.setState({problems:deleteRow?response.data.data:[...this.state.problems,...response.data.data],loading:false})
         })
         .catch(error=>{
          // console.log("ALL problems ERROR")
           //console.log(error.response)
         })
       }
     
       OKBUTTON = (e) => {
        this.deleteProblem()
       }
 
       deleteProblem = () => {
         let l = message.loading(allStrings.wait, 2.5)
         axios.delete(`${BASE_END_POINT}problem/${this.state.selectedProblem}`,{
           headers: {
             'Content-Type': 'application/x-www-form-urlencoded',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
         .then(response=>{
             l.then(() => message.success(allStrings.deleteDone, 2.5))
             this.getProblems(1,true)
             this.flag = 0
         })
         .catch(error=>{
             //console.log(error.response)
             l.then(() => message.error('Error', 2.5))
         })
      }

    //submit form
  
    render() {
      const{select} = this.props
         let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.yes}
            cancelText={allStrings.no}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
        
        
         let list = this.state.problems.map(val=>[""+val.id,""+val.problemType,""+val.description,val.reply?allStrings.yes:allStrings.no,controls
        ])
         const loadingView = [
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          
         ]
      return (
          <div>
               <AppMenu  height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
              <Tables 
              columns={this.state.loading?['Loading...']:[allStrings.id,allStrings.problemType, allStrings.description,allStrings.replay,allStrings.remove]} title={allStrings.problemstable}
              arr={this.state.loading?loadingView:list}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!==4){
                  console.log(colData)
                  console.log('row   ',cellMeta.rowIndex+"         "+cellMeta.dataIndex)
                  this.props.history.push('/ProblemInfo',{data:this.state.problems[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===4){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedProblem:id})
                    //console.log(id)
                  }
              }}

              onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{ 
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                console.log('current',currentPage)
              
                if(currentPage%2!=0  && currentPage > this.flag){
                  let rem = currentPage %2;
                  let x = currentPage - rem;
                  console.log(rem)
                  console.log(x)
                  this.flag  = this.flag +1;
                  console.log("flage",this.flag)
                    let page = rem + this.flag;
                    console.log("page",page)
                    this.getProblems(page)
                    console.log("request",page)
                    console.log("flag",this.flag) 

                } 
               

                  
              }}
              ></Tables>
              </div>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }


  export default withRouter( connect(mapToStateProps,mapDispatchToProps)  (Problem = Form.create({ name: 'normal_login' })(Problem)) );
 

