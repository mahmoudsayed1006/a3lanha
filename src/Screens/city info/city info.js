import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './city info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {  message,Modal, Form, Input,Icon} from 'antd';
import {Table} from 'react-materialize'

class CityInfo extends React.Component {
    state = {
        modal2Visible: false,
         cities:this.props.location.state.data,
        
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        


     deleteCity = () => {
      let l = message.loading(allStrings.wait, 2.5)
      axios.delete(`${BASE_END_POINT}countries/cities/${this.state.cities.id}`,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
      .then(response=>{
          l.then(() => message.success(allStrings.deleteDone, 2.5))
          this.props.history.push('/countries') 

      })
      .catch(error=>{
          console.log(error.response)
          l.then(() => message.error('Error', 2.5))
      })
   }

    
    handleSubmitAdd = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll(['cityName','arabicName'],(err, values) => {
        if (!err) {
          //console.log('Received values of form: ', values);
         var data ={
          cityName: values.cityName,
          arabicName: values.arabicName
         }
          
          let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}countries/cities/${this.state.cities.id}`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.push('/countries') 
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }


      setModal2Visible(modal2Visible) {
        this.setState({ modal2Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
        
         const {cities} = this.state  
        const {select} = this.props;

      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff',marginTop:"10px"}}>{allStrings.cityInfo}</h2>
                </div>
               
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={cities.id}>
                            </input>
                            <label for="name" class="active">{allStrings.id}</label>
                            </div>

                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={cities.cityName}>
                            </input>
                            <label for="name" class="active">{allStrings.EnglishCountryName}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={cities.arabicName}>
                            </input>
                            <label for="name" class="active">{allStrings.ArabicCountryName}</label>
                            </div>

                        </div>
                        
                       
                            <a className="waves-effect waves-light btn btn-large delete " onClick={this.deleteCity}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a className="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal2Visible(true)}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>
                       

                            
                            <div>
                            <Modal
                                title={allStrings.addCity}
                                visible={this.state.modal2Visible}
                                onOk={this.handleSubmitAdd}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal2Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmitAdd} className="login-form">

                                  <Form.Item>
                                  {getFieldDecorator('cityName', {
                                      rules: [{ required: true, message: 'Please enter city name by english' }],
                                      initialValue: cities.cityName
                                  })(
                                      <Input placeholder={allStrings.EnglishCityName}   />
                                  )}
                                  </Form.Item>

                                  <Form.Item>
                                  {getFieldDecorator('arabicName', {
                                      rules: [{ required: true, message: 'Please enter city name by arabic' }],
                                      initialValue: cities.arabicName
                                  })(
                                      <Input placeholder={allStrings.ArabicCityName}  />
                                  )}
                                  </Form.Item>
                           
                                
                                 
                              </Form>
                            </Modal>

                                
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(CityInfo = Form.create({ name: 'normal_login', })(CityInfo)) ;
