import React from 'react';
import Menu from '../../components/menu/menu';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './privacy.css';
import { Skeleton, message,Modal, Form,Spin, Icon, Input, Button,Popconfirm,Select} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
const { TextArea } = Input;


class Privacy extends React.Component {
  page = 0;
  pagentationPage=0;
  counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedCategory:null,
     terms:[],
     file:null,
     loading:true,
     tablePage:0,
     }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

     onChange = (e) => {
      this.setState({file:e.target.files[0]});
     
  }
    //submit form
    flag = -1;
    getTerms = () => {
      this.setState({loading:true})
      axios.get(`${BASE_END_POINT}about`)
      .then(response=>{
        console.log("ALL About")
        console.log(response.data)
       
        this.setState({terms:response.data,loading:false})
      })
      .catch(error=>{
        //console.log("ALL Categories ERROR")
        //console.log(error.response)
        this.setState({loading:false})
      })
    }
    componentDidMount(){
      this.getTerms()
    }
    
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
           
            const data =
             {
              about:this.state.terms[0].about,
              conditions:this.state.terms[0].conditions,
              privacy:values.terms ,
              usage:this.state.terms[0].usage
              
            }

           
            let l = message.loading(allStrings.wait, 2.5)
            axios.put(`${BASE_END_POINT}about/${this.state.terms[0].id}`,JSON.stringify(data),{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.addDone, 2.5));
                this.setState({ modal1Visible:false });
                this.getTerms()
            })
            .catch(error=>{
              //  console.log(error.response)
                //l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })
          }
        });
        
      }
     
    //end submit form

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;

          const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
           const {select} = this.props;
           console.log("user anwer    ",this.props.currentUser);
      return (
          <div className='conatiner'>
            {this.props.currentUser&&
              <AppMenu goTo={this.props.history}></AppMenu>            
            }
            {this.props.currentUser&&
              <Nav></Nav>            
            }
             
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
              
               
               <div className='termsContainerData'>
               <div className='termsContainer'>
                    <span className='termsTitle'>{allStrings.privacy}</span>
                  
               </div>
                    {this.state.loading?
                    <Spin indicator={antIcon} />
                    :
                    <span className='termsData'>{this.state.terms[0].privacy}</span>
                    }
               </div>

              {this.props.currentUser&&
               <button onClick={()=>this.setModal1Visible(true)} className='editTerms'>{allStrings.update}</button>
              }
              
                  
                
              <div>
              <Modal
                    title={allStrings.add}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                        {getFieldDecorator('terms', {
                            rules: [{ required: true, message: 'Please enter terms and condition' }],
                            initialValue:this.state.loading?'':this.state.terms[0].privacy
                        })(
                            <TextArea 
                            autosize={{ minRows: 10 }}
                            placeholder={allStrings.about} />
                           
                        )}
                        </Form.Item>

                       
                        
                        
                        
                    </Form>
                </Modal>
              </div> 
            </div>
             <Footer></Footer>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)( Privacy= Form.create({ name: 'normal_login' })(Privacy));
