import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './country info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {  message,Modal, Form, Input,Icon} from 'antd';
import {Table} from 'react-materialize'

class CountryInfo extends React.Component {
    state = {
        modal1Visible: false,
        modal2Visible: false,
         cities:[],
         country:this.props.location.state.data,
         flag:this.props.location.state.flag,
         file: null,
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
         onChange = (e) => {
            this.setState({file:e.target.files[0]});
        }
        getcities = () => {
          axios.get(`${BASE_END_POINT}countries/${this.state.country.id}/cities`)
          .then(response=>{
            console.log("ALL cities    ",response.data)
            console.log(response.data)
            
            this.setState({cities:response.data.data})
            console.log(this.state.cities)

          })
          .catch(error=>{
            console.log("ALL cities ERROR")
            console.log(error.response)
           
          })
        }
        
         componentDidMount()
         {
             
            this.getcities()
             
         }

    
    deletecountry = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}countries/${this.state.country.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }

     deleteCity = (id) => {
      let l = message.loading(allStrings.wait, 2.5)
      axios.delete(`${BASE_END_POINT}countries/cities/${id}`,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
      .then(response=>{
          l.then(() => message.success(allStrings.deleteDone, 2.5))
          this.props.history.goBack()

      })
      .catch(error=>{
          console.log(error.response)
          l.then(() => message.error('Error', 2.5))
      })
   }

     handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll(['countryName','arabicName','countryCode','currency'],(err, values)  => {
        if (!err) {
          //console.log('Received values of form: ', values);
          var form = new FormData();
          if(this.state.file){
            form.append('img', this.state.file);
          }
          
          form.append('countryName', values.countryName);
          form.append('arabicName', values.arabicName);
          form.append('countryCode', values.countryCode);
            form.append('currency', values.currency);
    
          let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}countries/${this.state.country.id}`,form,{
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }
    
    handleSubmitAdd = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll(['cityName','arabicName'],(err, values) => {
        if (!err) {
          //console.log('Received values of form: ', values);
         var data ={
          cityName: values.cityName,
          arabicName: values.arabicName
         }
          
          let l = message.loading(allStrings.wait, 2.5)
          axios.post(`${BASE_END_POINT}countries/${this.state.country.id}/cities`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }


      
      //modal
     
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
      setModal2Visible(modal2Visible) {
        this.setState({ modal2Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const {country} = this.state
         const {cities} = this.state
         console.log(cities)
  
        const {select} = this.props;
      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff',marginTop:"10px"}}>{allStrings.countryInfo}</h2>
                </div>
                <div class="row">
                <Carousel images={country.img} />
                </div>
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.countryName}>
                            </input>
                            <label for="name" class="active">{allStrings.EnglishCountryName}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.arabicName}>
                            </input>
                            <label for="name" class="active">{allStrings.ArabicCountryName}</label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.countryCode}>
                            </input>
                            <label for="name" class="active">{allStrings.countryCode}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.currency}>
                            </input>
                            <label for="name" class="active">{allStrings.currency}</label>
                            </div>

                        </div>
                        <div className='dash-table'>
                            <h5>{allStrings.cities} : </h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="name">{allStrings.cityName}</th>
                                            <th data-field="arabic">{allStrings.cityArabicName}</th>
                                            <th>{allStrings.remove}</th>
                                            </tr>
                                        </thead> 

                                        <tbody>
                                        {cities.map(val=>(
                                            <tr>
                                                <td  onClick={()=>{this.props.history.push('/CityInfo',{data:val}) }} >{val.id}</td>
                                                <td  onClick={()=>{this.props.history.push('/CityInfo',{data:val}) }} >{val.cityName}</td>                                 
                                                <td  onClick={()=>{this.props.history.push('/CityInfo',{data:val}) }} >{val.arabicName}</td>
                                                <td 
                                                onClick={()=>{this.deleteCity(val.id)}}
                                                 ><Icon className='controller-icon' type="delete" style={{ fontSize: "16px"}}/></td>
                                            </tr>
                                        ))}

                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div> 

                        
                       
                            <a className="waves-effect waves-light btn btn-large delete " onClick={this.deletecountry}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a className="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>
                            <a style={{background:"#2bbbad"}} className="waves-effect waves-light btn btn-large " onClick={() => this.setModal2Visible(true)}><i class="material-icons left spcial">add</i>{allStrings.addCity}</a>



                            <div>
                            <Modal
                                title={allStrings.edit}
                                visible={this.state.modal1Visible}
                                onOk={this.handleSubmit}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal1Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmit} className="login-form">

                               <label for="name" className="lab">{allStrings.EnglishCountryName}</label>
                                  <Form.Item>
                                  {getFieldDecorator('countryName', {
                                      rules: [{ required: true, message: 'Please enter country name by english' }],
                                      initialValue: country.countryName
                                  })(
                                      <Input type='text' />
                                  )}
                                  </Form.Item>

                                  <label for="name" className="lab">{allStrings.ArabicCountryName}</label>
                                  <Form.Item>
                                  {getFieldDecorator('arabicName', {
                                      rules: [{ required: true, message: 'Please enter country name by arabic' }],
                                      initialValue: country.arabicName
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>

                                  <label for="name" className="lab">{allStrings.countryCode}</label>
                                  <Form.Item>
                                  {getFieldDecorator('countryCode', {
                                      rules: [{ required: true, message: 'Please enter country code' }],
                                      initialValue: country.countryCode
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>

                                  <label for="name" className="lab">{allStrings.currency}</label>
                                  <Form.Item>
                                  {getFieldDecorator('currency', {
                                      rules: [{ required: true, message: 'Please enter currency' }],
                                      initialValue: country.currency
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>

                                 
                           
                                 
                                 
                              </Form>
                              <input className='file' type="file" onChange= {this.onChange} multiple></input>
                            </Modal>

                                
                        
                            </div>

                            
                            <div>
                            <Modal
                                title={allStrings.addCity}
                                visible={this.state.modal2Visible}
                                onOk={this.handleSubmitAdd}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal2Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmitAdd} className="login-form">

                                  <Form.Item>
                                  {getFieldDecorator('cityName', {
                                      rules: [{ required: true, message: 'Please enter city name by english' }],
                                      initialValue: ''
                                  })(
                                      <Input placeholder={allStrings.EnglishCityName}   />
                                  )}
                                  </Form.Item>

                                  <Form.Item>
                                  {getFieldDecorator('arabicName', {
                                      rules: [{ required: true, message: 'Please enter city name by arabic' }],
                                      initialValue: ''
                                  })(
                                      <Input placeholder={allStrings.ArabicCityName}  />
                                  )}
                                  </Form.Item>
                           
                                
                                 
                              </Form>
                            </Modal>

                                
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(CountryInfo = Form.create({ name: 'normal_login', })(CountryInfo)) ;
