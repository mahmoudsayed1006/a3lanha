import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './suggestion info.css';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {  message,Modal, Form, Input,Icon} from 'antd';
import {Table} from 'react-materialize'

class SuggestionInfo extends React.Component {
    state = {
        modal2Visible: false,
         suggestions:this.props.location.state.data,
        
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        


     deleteSuggestion = () => {
      let l = message.loading(allStrings.wait, 2.5)
      axios.delete(`${BASE_END_POINT}suggestion/${this.state.suggestions.id}`,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
      .then(response=>{
          l.then(() => message.success(allStrings.deleteDone, 2.5))
          this.props.history.push('/suggestions') 

      })
      .catch(error=>{
          console.log(error.response)
          l.then(() => message.error('Error', 2.5))
      })
   }
   
    render() {
         //select
        
         const {suggestions} = this.state  
        const {select} = this.props;

      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff',marginTop:"10px"}}>{allStrings.suggestionInfo}</h2>
                </div>
               
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={suggestions.id}>
                            </input>
                            <label for="name" class="active">{allStrings.id}</label>
                            </div>

                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={suggestions.description}>
                            </input>
                            <label for="name" class="active">{allStrings.description}</label>
                            </div>

                        </div>
                            <a className="waves-effect waves-light btn btn-large delete " onClick={this.deleteSuggestion}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>

                          
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(SuggestionInfo = Form.create({ name: 'normal_login', })(SuggestionInfo)) ;
