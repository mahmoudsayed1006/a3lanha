import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './ads info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {  message,Modal, Form, Input,Icon} from 'antd';
import {Table} from 'react-materialize'
import GoogleMapReact from 'google-map-react';

class AdsInfo extends React.Component {
    state = {
        modal1Visible: false,
        modal2Visible: false,
         cities:[],
         country:this.props.location.state.data,
         flag:this.props.location.state.flag,
         file: this.props.location.state.data.img,
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
         onChange = (e) => {
            this.setState({file:e.target.files[0]});
        }
        getcities = () => {
          axios.get(`${BASE_END_POINT}countries/${this.state.country.id}/cities`)
          .then(response=>{
            console.log("ALL cities    ",response.data)
            console.log(response.data)
            
            this.setState({cities:response.data.data})
            console.log(this.state.cities)

          })
          .catch(error=>{
            console.log("ALL cities ERROR")
            console.log(error.response)
           
          })
        }
        
         componentDidMount()
         {
             
            this.getcities()
             
         }

    
    deletecountry = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}ads/${this.state.country.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }

     agreeAds = () => {
      let l = message.loading(allStrings.wait, 2.5)
      axios.put(`${BASE_END_POINT}ads/${this.state.country.id}/active`,{},{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
      .then(response=>{
          l.then(() => message.success(allStrings.active, 2.5))
          this.props.history.goBack()

      })
      .catch(error=>{
          console.log(error.response)
          l.then(() => message.error('Error', 2.5))
      })
   }

   disagreeAds = () => {
    let l = message.loading(allStrings.wait, 2.5)
    axios.put(`${BASE_END_POINT}ads/${this.state.country.id}/dis-active`,{},{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.token}`
      },
    })
    .then(response=>{
        l.then(() => message.success(allStrings.disactive, 2.5))
        this.props.history.goBack()

    })
    .catch(error=>{
        console.log(error.response)
        l.then(() => message.error('Error', 2.5))
    })
 }

 isTop = (top) => {
   var uri=''
   if(top){
    uri=`${BASE_END_POINT}ads/${this.state.country.id}/top`
   }else{
    uri = `${BASE_END_POINT}ads/${this.state.country.id}/low`
   }
  let l = message.loading(allStrings.wait, 2.5)
  axios.put(uri,{},{
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.props.currentUser.token}`
    },
  })
  .then(response=>{
      if(top){
        l.then(() => message.success(allStrings.adsBecomeTop, 2.5))

      }else{
        l.then(() => message.success(allStrings.adsBecomeLow, 2.5))

      }
      this.props.history.goBack()

  })
  .catch(error=>{
      console.log(error.response)
      l.then(() => message.error('Error', 2.5))
  })
}

     handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          //console.log('Received values of form: ', values);
          var form = new FormData();
          if(this.state.file){
            form.append('img', this.state.file);
          }
          
          form.append('countryName', values.countryName);
          form.append('arabicName', values.arabicName);
    
          let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}countries/${this.state.country.id}`,form,{
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }
    handleSubmitAdd = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          //console.log('Received values of form: ', values);
          var form = new FormData();
         
          
          form.append('cityName', values.countryName);
          form.append('arabicName', values.arabicName);
          let l = message.loading(allStrings.wait, 2.5)
          axios.post(`${BASE_END_POINT}countries/${this.state.country.id}/cities`,form,{
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }


      
      //modal
     
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
      setModal2Visible(modal2Visible) {
        this.setState({ modal2Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const {country} = this.state
         const {cities} = this.state
         console.log(cities)
  
        const {select} = this.props;
        const MarkerMap = ({ color }) =>
        <span class="material-icons" style={{color:color,fontSize:50}}>
        location_on
        </span>

      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff',marginTop:"10px"}}>{allStrings.adsInfo}</h2>
                </div>
                <div class="row">
                <Carousel options={{duration: 15}} images={country.img} />
                </div>
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.id}>
                            </input>
                            <label for="name" class="active">{allStrings.id}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.title}>
                            </input>
                            <label for="name" class="active">{allStrings.title}</label>
                            </div>

                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.description}>
                            </input>
                            <label for="name" class="active">{allStrings.description}</label>
                            </div>


                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.category.name=='Jops'?country.priceFrom+" - "+country.priceTo:""+country.price}>
                            </input>
                            <label for="name" class="active">{allStrings.price}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.address}>
                            </input>
                            <label for="name" class="active">{allStrings.address}</label>
                            </div>

                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.category.name}>
                            </input>
                            <label for="name" class="active">{allStrings.EnglishCategoryName}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.category.arabicName}>
                            </input>
                            <label for="name" class="active">{allStrings.ArabicCategoryName}</label>
                            </div>

                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.subCategory.name}>
                            </input>
                            <label for="name" class="active">{allStrings.englishSubCategoryName}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.subCategory.arabicName}>
                            </input>
                            <label for="name" class="active">{allStrings.arabicSubCategoryName}</label>
                            </div>

                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.visible}>
                            </input>
                            <label for="name" class="active">{allStrings.active}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.top}>
                            </input>
                            <label for="name" class="active">{allStrings.isTop}</label>
                            </div>

                            
                        </div>

                        
                        {country.category.name=='JOPS'&&

                          <div class="row">
                              <div class="input-field col s6">
                              <input id="name" type="text" class="validate" disabled value={country.jopRequirements}>
                              </input>
                              <label for="name" class="active">{allStrings.jobReuirements}</label>
                              </div>

                              <div class="input-field col s6">
                              <input id="name" type="text" class="validate" disabled value={country.jopType}>
                              </input>
                              <label for="name" class="active">{allStrings.jobType}</label>
                              </div>

                          </div>

                        }

                    {country.category.name=='JOPS'&&

                    <div class="row">
                        <div class="input-field col s6">
                        <input id="name" type="text" class="validate" disabled value={country.experience}>
                        </input>
                        <label for="name" class="active">{allStrings.experience}</label>
                        </div>

                        <div class="input-field col s6">
                        <input id="name" type="text" class="validate" disabled value={country.salarySystem}>
                        </input>
                        <label for="name" class="active">{allStrings.salarySystem}</label>
                        </div>

                    </div>

                    }

                  {country.category.type=='JOPS'&&

                  <div class="row">
                      <div class="input-field col s6">
                      <input id="name" type="text" class="validate" disabled value={country.companyName}>
                      </input>
                      <label for="name" class="active">{allStrings.companyName}</label>
                      </div>

                      <div class="input-field col s6">
                      <input id="name" type="text" class="validate" disabled value={country.date}>
                      </input>
                      <label for="name" class="active">{allStrings.date}</label>
                      </div>

                  </div>

                  }

                  {/*    */}

                      {country.category.type=='REAL-STATE'&&
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.beds}>
                            </input>
                            <label for="name" class="active">{allStrings.beds}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.baths}>
                            </input>
                            <label for="name" class="active">{allStrings.bathes}</label>
                            </div>

                        </div>

                        }

                        {country.category.type=='REAL-STATE'&&
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.space}>
                            </input>
                            <label for="name" class="active">{allStrings.space}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.rental}>
                            </input>
                            <label for="name" class="active">{allStrings.rental}</label>
                            </div>

                        </div>

                        }
                         {country.category.type=='MOTOR'&&
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.brand}>
                            </input>
                            <label for="name" class="active">{allStrings.brand}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.modal}>
                            </input>
                            <label for="name" class="active">{allStrings.modelname}</label>
                            </div>

                        </div>
                        

                        }
                         {country.category.type=='MOTOR'&&
                          <div class="row">
                          <div class="input-field col s6">
                          <input id="name" type="text" class="validate" disabled value={country.usage}>
                          </input>
                          <label for="name" class="active">{allStrings.usage}</label>
                          </div>

                          <div class="input-field col s6">
                          <input id="name" type="text" class="validate" disabled value={country.year}>
                          </input>
                          <label for="name" class="active">{allStrings.year}</label>
                          </div>

                        </div>
                        
                        }
                         {country.category.type=='MOTOR'&&
                         <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={country.mileage}>
                            </input>
                            <label for="name" class="active">{allStrings.mileage}</label>
                            </div>

                            <div class="input-field col s6">
                            <img src={country.color.img} width="50px" height="50px" alt='color' style={{borderRadius:'50% !important'}}></img>
                            <label for="name" class="active">{allStrings.color}</label>
                            </div>
                          </div>
                        
                        }

                        

                        <div className='dash-table'>
                            <h5>{allStrings.owner} : </h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="title">{allStrings.name}</th>
                                            <th data-field="price">{allStrings.email}</th>
                                            <th data-field="price">{allStrings.phone}</th>
                                            
                                           
                                            </tr>
                                        </thead>  

                                        <tbody>
                                        <tr onClick={()=>{this.props.history.push('/UserInfo',{data:country.owner}) }} >
                                                <td>{country.owner.id}</td>
                                                <td>{country.owner.username}</td>
                                                <td>{country.owner.email}</td>
                                                <td>{country.owner.phone}</td>
                                        </tr>

                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div> 
                        

                        
                        <div style={{marginBottom:20, height:300, width: '100%' }}>
                        <GoogleMapReact
                          bootstrapURLKeys={{ key: "AIzaSyAy_TzJaYpsCRQ-x6rXwOdqEqHr6XaU8pY" }}
                          defaultCenter={{
                            lat: country.location[0],
                            lng: country.location[1]
                          }}
                          defaultZoom={10}
                        >
                         <MarkerMap
                          lat={country.location[0]}
                          lng={country.location[1]}
                            text="My Marker"
                            color='red'
                          />
                        </GoogleMapReact>
                        </div>
                       
                            <a className="waves-effect waves-light btn btn-large delete " onClick={this.deletecountry}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            {
                              country.visible?
                              <a className="waves-effect waves-light btn btn-large edit " onClick={this.disagreeAds}>{allStrings.disactive}</a>
                              :
                              <a className="waves-effect waves-light btn btn-large edit " onClick={this.agreeAds}>{allStrings.active}</a>

                            }

                        <a className="waves-effect waves-light btn btn-large edit " onClick={()=>this.isTop(true)}>{allStrings.top}</a>

                        <a className="waves-effect waves-light btn btn-large edit " onClick={()=>this.isTop(false)}>{allStrings.low}</a>




                           

                          
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(AdsInfo = Form.create({ name: 'normal_login', })(AdsInfo)) ;
