import React from 'react';
  import Menu from '../../components/menu/menu';
  import AppMenu from '../../components/menu/menu';
  
  import Nav from '../../components/navbar/navbar';
  import Footer from '../../components/footer/footer';
  import {Carousel} from 'react-materialize';
  import './slider.css';
  import { Skeleton, message,Modal, Form,Spin, Icon, Input, Button,Popconfirm,Select} from 'antd';
  import "antd/dist/antd.css";
  import axios from 'axios';
  import {BASE_END_POINT} from '../../config/URL'
  import { connect } from 'react-redux';
  import  {allStrings} from '../../assets/strings'
  
  const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
  const { TextArea } = Input;
  
  
  class Slider extends React.Component {
    page = 0;
    pagentationPage=0;
    counter=0;
    state = {
      modal1Visible: false,
      confirmDelete: false,
       slider:[],
       file:null,
       loading:true,
       tablePage:0,
       }
  
       constructor(props){
        super(props)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
      }
  
       onChange = (e) => {
        this.setState({file:e.target.files[0]});
       
    }
      //submit form
      flag = -1;
      getSlider = () => {
        this.setState({loading:true})
        axios.get(`${BASE_END_POINT}slider`)
        .then(response=>{
          console.log("ALL About")
          console.log(response.data)
         
          this.setState({slider:response.data.data[0],loading:false})
        })
        .catch(error=>{
          //console.log("ALL Categories ERROR")
          //console.log(error.response)
          this.setState({loading:false})
        })
      }
      componentDidMount(){
        this.getSlider()
      }
      
      handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll(['title'],(err, values)  => {
          if (!err) {
            //console.log('Received values of form: ', values);
            var form = new FormData();
            if(this.state.file){
              form.append('img', this.state.file);
            }
            
            form.append('title', values.title);
           
      
            let l = message.loading(allStrings.wait, 2.5)
            axios.put(`${BASE_END_POINT}slider/${this.state.slider.id}`,form,{
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.updatedDone, 2.5))
                this.props.history.goBack()
            })
            .catch(error=>{
             //   console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })
          }
        });
        
      }
       
      //end submit form
  
        setModal1Visible(modal1Visible) {
          this.setState({ modal1Visible });
        }
  
  //end modal
      render() {
          //form
           const { getFieldDecorator } = this.props.form;
           const {slider} = this.state
           console.log(slider)
           console.log(slider.img)
           var image = [
            'https://picsum.photos/200/300?image=0',
            'https://picsum.photos/200/300?image=1',
            'https://picsum.photos/200/300?image=2',
          ]
           console.log(image)

             const {select} = this.props;
        return (

            <div className='conatiner'>
              {this.props.currentUser&&
                <AppMenu goTo={this.props.history}></AppMenu>            
              }
              {this.props.currentUser&&
                <Nav></Nav>            
              }
               
                <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
                <div class="row">
                  
                <Carousel  images={slider.img} />

                </div>
                 
                
  
                {this.props.currentUser&&
                  <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:20,borderRadius: '50%',width:'80px',height:'80px'}} onClick={()=>this.setModal1Visible(true)} >{allStrings.update}</Button>
  
                }
                
                    
                  
                <div>
                <Modal
                      title={allStrings.edit}
                      visible={this.state.modal1Visible}
                      onOk={this.handleSubmit}
                      onCancel={() => this.setModal1Visible(false)}
                      okText={allStrings.ok}
                      cancelText={allStrings.cancel}
                    >
                      <Form onSubmit={this.handleSubmit} className="login-form">
                          <Form.Item>
                          {getFieldDecorator('title', {
                              rules: [{ required: true, message: 'Please enter title' }],
                              initialValue:this.state.loading?'':slider.title
                          })(
                              <TextArea 
                              placeholder={allStrings.title} />
                             
                          )}
                          </Form.Item>
                          
                      </Form>
                      <input className='file' type="file" onChange= {this.onChange} multiple></input>
  
                  </Modal>
                </div> 
              </div>
               <Footer></Footer>
            </div>
        );
      }
    }
  
    const mapToStateProps = state => ({
      isRTL: state.lang.isRTL,
      currentUser: state.auth.currentUser,
      select: state.menu.select,
  
    })
    
    const mapDispatchToProps = {
    }
  
    export default  connect(mapToStateProps,mapDispatchToProps)( Slider= Form.create({ name: 'normal_login' })(Slider));
  