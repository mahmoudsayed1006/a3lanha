import React, { Component } from 'react';
import Menu from '../../components/menu/menu';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './admin info.css';
import {Button,Card,Input,CardTitle} from 'react-materialize';
import "antd/dist/antd.css";
 import { Modal,Form ,Select ,DatePicker,message,Icon} from 'antd';
 import axios from 'axios';
 import {BASE_END_POINT} from '../../config/URL'
import {Table} from 'react-materialize'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
import {getUser} from '../../actions/AuthActions'



class AdminInfo extends React.Component {
       //submit form
       page=1;
       type=null;
       state = {
        modal1Visible: false,
        user:this.props.location.state.data,
        file:null,
        ordermodal:false,
        Ads:[],
        loading:false,
        selectFlag:false,
        countries:[],
        //this.props.location.state.data.img[0],
        
      }

      constructor(props){
        super(props)
        this.getCountries()
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
      }

      getCountries = () => {
        axios.get(`${BASE_END_POINT}countries`)
        .then(response=>{
          this.setState({countries:response.data.data})
        })
        .catch(error=>{
          //console.log("ALL Categories ERROR")
          //console.log(error.response)
        })
      }

      getAds= (page,type,reload) => {
        console.log("page  ",page)
        axios.get(`${BASE_END_POINT}ads?owner=${this.state.user.id}&page=${page}&limit={20}`)
        .then(response=>{
          console.log("ALL Ads")
          console.log(response.data.data)
          this.setState({selectFlag:true,Ads:reload?response.data.data : [...this.state.Ads,...response.data.data],loading:false})
        })
        .catch(error=>{
          console.log("ALL orders ERROR")
          console.log(error.response)
          this.setState({loading:false})
        })
      }

      onChange = (e) => {
        this.setState({file:e.target.files[0]});
    }

       componentDidMount()
       {
           this.getAds(this.page)
       }

       deleteUser = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}${this.state.user.id}/delete`,{
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()
        })
        .catch(error=>{
            //console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }


       block = (active) => {
           let uri ='';
           if(active){
            uri = `${BASE_END_POINT}${this.state.user.id}/block`
           }else{
            uri = `${BASE_END_POINT}${this.state.user.id}/unblock`
           }
          let l = message.loading(allStrings.wait, 2.5)
           axios.put(uri,{},{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
           .then(response=>{
               // console.log('done')
                if(active){
                    
                    
                    l.then(() => message.success(allStrings.blockDone, 2.5))
                    
                }else{
                
                  l.then(() => message.success(allStrings.unblockDone, 2.5))
                }
                this.props.history.goBack()
           })
           .catch(error=>{
           // console.log('Error')
           // console.log(error.response)
            l.then(() => message.error('Error', 2.5))
           })
       }

     

       handleSubmit = (e) => {
        e.preventDefault();
        console.log('user ID     ',this.state.user.id)
        this.props.form.validateFields((err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
            var form = new FormData();
            if(this.state.file){
                form.append('img',this.state.file);
            }
            form.append('username', values.username);
            form.append('email', values.email);
            form.append('phone', values.phone);
            form.append('country', values.country.key);
            let l = message.loading(allStrings.wait, 2.5)
            axios.put(`${BASE_END_POINT}user/${this.state.user.id}/updateInfo`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.updatedDone, 2.5));           
                const user = {...this.props.currentUser,user:{...response.data.user}}
                //localStorage.setItem('test', JSON.stringify(user));
                localStorage.setItem('@QsathaUser', JSON.stringify(user));  
                this.props.getUser(user);
                console.log("update      ",response.data)
                this.setState({ modal1Visible:false });
                this.props.history.goBack()
            })
            .catch(error=>{
                console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })
          }
        });
       
        
      }

    //end submit form
      
      //modal
    
  
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
  

  //end modal
  
    render() {
        const { getFieldDecorator } = this.props.form;
        const {user} = this.state;
        const {Ads} = this.state;
         //select
         const Option = Select.Option;

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
         const {select} = this.props;
 
      return (
          
        <div>
         <AppMenu height={'200%'} goTo={this.props.history} />
        <Nav></Nav>
        <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}}>
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.personalInfo}</h2>
                </div>
                <div class="row">
                
                    <form class="col s12">
                    <img style={{borderColor:'transparent'}} src={ 'img' in user?user.img:"https://sophosnews.files.wordpress.com/2013/08/facebook-silhouette_thumb.jpg?w=250"}></img>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="username" type="text" class="validate" disabled value={user.username}>
                            </input>
                            <label for="usertname" class="active">{allStrings.name}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="email" type="text" class="validate" disabled value={user.phone}>
                            </input>
                            <label for="email" class="active">{allStrings.phone}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="country" type="text" class="validate" disabled value={user.country}>
                            </input>
                            <label for="country" class="active">{allStrings.country}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="phone" type="text" class="validate" disabled value={user.email}></input>
                            <label for="phone" class="active">{allStrings.email}</label>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.id}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.id}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.block}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.block}</label>
                            </div>
                           
                        </div>
                  

                                
                        <div>
                       
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class=" spcial material-icons left">edit</i>{allStrings.edit}</a>
                        </div>
                       
                                                                             
                        </form>
                    <Modal
                            title="Edit"
                            visible={this.state.modal1Visible}
                            onOk={this.handleSubmit}
                            okText={allStrings.ok}
                            cancelText={allStrings.cancel}
                            onCancel={() => this.setModal1Visible(false)}
                        >
                            
                            <Form onSubmit={this.handleSubmit} className="login-form">

                             <label for="name" class="lab">{allStrings.name}</label>
                            <Form.Item>
                            {getFieldDecorator('username', {
                                rules: [{ required: true, message: 'Please enter username' }],
                                initialValue: user.username,
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.email}</label>
                            <Form.Item>
                            {getFieldDecorator('email', {
                                rules: [{ required: true, message: 'Please enter email' }],
                                initialValue:user.email
                            })(
                                <Input />
                            )}
                            </Form.Item>
                            <label for="name" class="lab">{allStrings.country}</label>
                            <Form.Item>
                    {getFieldDecorator('country', {
                        rules: [{ required: true, message: 'Please enter country' }],
                    })(
                      <Select labelInValue  
                      placeholder={allStrings.country}
                      style={{ width: '100%'}} >
                           {this.state.countries.map(val=>
                           <Option value={val.countryName}>{this.props.isRTL?val.arabicName:val.countryName}</Option>
                           )}                 
                      </Select>
                    )}
                    </Form.Item>
                        
                            <label for="name" class="lab">{allStrings.phone}</label>
                            <Form.Item>
                            {getFieldDecorator('phone', {
                                rules: [{ required: true, message: 'Please enter phone' }],
                                initialValue:user.phone
                            })(
                                <Input/>
                            )}
                            </Form.Item>


                            </Form>
                            <label for="name" class="lab">{allStrings.personalImage}</label>
                            <br/>
                            <input className='profileImg' type="file" onChange= {this.onChange}></input>
                        </Modal>
                             
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  
  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
    getUser,
  }


export default connect(mapToStateProps,mapDispatchToProps) ( AdminInfo = Form.create({ name: 'normal_login' })(AdminInfo)) ;
