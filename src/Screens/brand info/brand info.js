import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './brand info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {  message,Modal, Form, Input,Icon} from 'antd';
import {Table} from 'react-materialize'

class BrandInfo extends React.Component {
    state = {
        modal1Visible: false,
        modal2Visible: false,
         models:[],
         Brand:this.props.location.state.data,
         flag:this.props.location.state.flag,
         file: null,
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
         onChange = (e) => {
            this.setState({file:e.target.files[0]});
        }
        getmodels = () => {
          axios.get(`${BASE_END_POINT}model/${this.state.Brand.id}/brands`)
          .then(response=>{
            console.log("ALL models    ",response.data)
            console.log(response.data)
            
            this.setState({models:response.data})
            console.log(this.state.models)

          })
          .catch(error=>{
            console.log("ALL models ERROR")
            console.log(error.response)
           
          })
        }
        
         componentDidMount()
         {
             
            this.getmodels()
             
         }

    
    deleteBrand = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}brand/${this.state.Brand.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }

     deletemodel = (id) => {
      let l = message.loading(allStrings.wait, 2.5)
      axios.delete(`${BASE_END_POINT}/model/${id}`,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
      .then(response=>{
          l.then(() => message.success(allStrings.deleteDone, 2.5))
          this.props.history.goBack()

      })
      .catch(error=>{
          console.log(error.response)
          l.then(() => message.error('Error', 2.5))
      })
   }

     handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll(['brandname'],(err, values)  => {
        if (!err) {
          //console.log('Received values of form: ', values);
          var form = new FormData();
          if(this.state.file){
            form.append('img', this.state.file);
          }
          
          form.append('brandname', values.brandname);
        
    
          let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}brand/${this.state.Brand.id}`,form,{
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }
    
    handleSubmitAdd = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll(['modelname'],(err, values) => {
        if (!err) {
          //console.log('Received values of form: ', values);
         var data ={
          modelname: values.modelname,
          brand:this.state.Brand.id,
         }
          
          let l = message.loading(allStrings.wait, 2.5)
          axios.post(`${BASE_END_POINT}model/`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }


      
      //modal
     
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
      setModal2Visible(modal2Visible) {
        this.setState({ modal2Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const {Brand} = this.state
         const {models} = this.state
         console.log(models)
  
        const {select} = this.props;
      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff',marginTop:"10px"}}>{allStrings.BrandInfo}</h2>
                </div>
                <div class="row">
                <Carousel images={Brand.img} />
                </div>
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Brand.brandname}>
                            </input>
                            <label for="name" class="active">{allStrings.brandname}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Brand.id}>
                            </input>
                            <label for="name" class="active">{allStrings.id}</label>
                            </div>

                        </div>
                       
                        <div className='dash-table'>
                            <h5>{allStrings.models} : </h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="name">{allStrings.modelname}</th>
                                            <th>{allStrings.remove}</th>
                                            </tr>
                                        </thead> 

                                        <tbody>
                                        {models.map(val=>(
                                            <tr>
                                                <td  onClick={()=>{this.props.history.push('/modelInfo',{data:val}) }} >{val.id}</td>
                                                <td  onClick={()=>{this.props.history.push('/modelInfo',{data:val}) }} >{val.modelname}</td>                                 
                                                <td 
                                                onClick={()=>{this.deletemodel(val.id)}}
                                                 ><Icon className='controller-icon' type="delete" style={{ fontSize: "16px"}}/></td>
                                            </tr>
                                        ))}

                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div> 

                        
                       
                            <a className="waves-effect waves-light btn btn-large delete " onClick={this.deleteBrand}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a className="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>
                            <a style={{background:"#2bbbad"}} className="waves-effect waves-light btn btn-large " onClick={() => this.setModal2Visible(true)}><i class="material-icons left spcial">add</i>{allStrings.addModel}</a>



                            <div>
                            <Modal
                                title={allStrings.edit}
                                visible={this.state.modal1Visible}
                                onOk={this.handleSubmit}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal1Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmit} className="login-form">

                               <label for="name" className="lab">{allStrings.brandname}</label>
                                  <Form.Item>
                                  {getFieldDecorator('brandname', {
                                      rules: [{ required: true, message: 'Please enter Brand name by english' }],
                                      initialValue: Brand.brandname
                                  })(
                                      <Input type='text' />
                                  )}
                                  </Form.Item>

                                 
                              </Form>
                              <input className='file' type="file" onChange= {this.onChange} multiple></input>
                            </Modal>

                                
                        
                            </div>

                            
                            <div>
                            <Modal
                                title={allStrings.addModel}
                                visible={this.state.modal2Visible}
                                onOk={this.handleSubmitAdd}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal2Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmitAdd} className="login-form">

                                  <Form.Item>
                                  {getFieldDecorator('modelname', {
                                      rules: [{ required: true, message: 'Please enter model name ' }],
                                      initialValue: ''
                                  })(
                                      <Input placeholder={allStrings.modelname}   />
                                  )}
                                  </Form.Item>
                           
                                
                                 
                              </Form>
                            </Modal>

                                
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(BrandInfo = Form.create({ name: 'normal_login', })(BrandInfo)) ;
