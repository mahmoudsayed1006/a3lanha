import React from 'react';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './color.css';
import { Skeleton, message,Modal, Form, Icon, Input, Button,Popconfirm} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class Color extends React.Component {
  page = 0;
  pagentationPage=0;
  counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedColor:null,
     colors:[],
     parents:[],
     file:null,
     loading:true,
     tablePage:0,
     }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

     onChange = (e) => {
      this.setState({file:e.target.files[0]});
     
  }
    //submit form
    flag = 0;
    getColors = (page,deleteRow) => {
      axios.get(`${BASE_END_POINT}color?page=${page}&limit=20`)
      .then(response=>{
        console.log("ALL colors    ",response.data.data)
        console.log(response.data)
        if(deleteRow){
          this.setState({tablePage:0})
         }
         this.setState({colors:deleteRow?response.data.data:[...this.state.colors,...response.data.data],loading:false})
        })
      .catch(error=>{
        console.log("ALL colors ERROR")
        console.log(error.response)
        this.setState({loading:false})
      })
    }
    
    componentDidMount(){
      this.getColors(1,true)
    }
    OKBUTTON = (e) => {
      this.deleteColor()
     }

     deleteColor = () => {
       let l = message.loading(allStrings.wait, 2.5)
       axios.delete(`${BASE_END_POINT}color/${this.state.selectedColor}`,{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
       .then(response=>{
           l.then(() => message.success(allStrings.deleteDone, 2.5))
           this.getColors(1,true)
           this.flag = -1
       })
       .catch(error=>{
          // console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            //console.log('Received values of form: ', values);
            var form = new FormData();
            form.append('img',this.state.file);
            form.append('colorName', values.colorName);
            form.append('arabicColorName', values.arabicColorName);
           
            let l = message.loading(allStrings.wait, 2.5)
            axios.post(`${BASE_END_POINT}color`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.addDone, 2.5));
                this.setState({ modal1Visible:false,file:null });
                this.fileInput.value = "";
                this.getColors(1,true)
                this.flag = -1;
                this.props.form.resetFields()
            })
            .catch(error=>{
                console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })
          }
        });
        
      }
     
    //end submit form

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
       
          let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
         let list =this.state.colors.map((val,index)=>[
          val.id,val.colorName,val.arabicColorName,
          <div>
            <img src={val.img} width="50px" height="50px" alt='anoncement'></img>
          </div>,
          
          controls
        ])
         

          const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
           const {select} = this.props;
      return (
          <div>
              <AppMenu height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
              <Tables
               columns={this.state.loading?['loading...']: [allStrings.id,allStrings.colorName,allStrings.arabicColorName,allStrings.image, allStrings.remove]} 
              title={allStrings.ColorTable}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!==4){
                  
                  //console.log(this.state.countries[cellMeta.rowIndex])
                  this.props.history.push('/colorInfo',{data:this.state.colors[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===4){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedColor:id})
                    //console.log(id)
                  }
              }}
              onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{ 
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                console.log('current',currentPage)
              
                if(currentPage%2!=0  && currentPage > this.flag){
                  let rem = currentPage %2;
                  let x = currentPage - rem;
                  console.log(rem)
                  console.log(x)
                  this.flag  = this.flag +1;
                  console.log("flage",this.flag)
                    let page = rem + this.flag;
                    console.log("page",page)
                    this.getColors(page)
                    console.log("request",page)
                    console.log("flag",this.flag) 

                } 
               

                  
              }}
              arr={this.state.loading?loadingView:list}></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addColor}</Button>
              <Modal
                    title={allStrings.addColor}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                        {getFieldDecorator('colorName', {
                            rules: [{ required: true, message: 'Please enter english color name' }],
                        })(
                            <Input placeholder={allStrings.colorName}/>
                        )}
                        </Form.Item>
                        <Form.Item>
                        {getFieldDecorator('arabicColorName', {
                            rules: [{ required: true, message: 'Please enter arabic color name' }],
                        })(
                            <Input placeholder={allStrings.arabicColorName}/>
                        )}
                        </Form.Item>

                        <Form.Item>
                        {getFieldDecorator('img', {
                            rules: [{ required: true, message: 'Please upload img' }],
                        })(
                          <input  ref={ref=> this.fileInput = ref} style={{width:200,margin:'auto',backgroundColor:'transparent',borderRadius:10}} type="file" onChange= {this.onChange} multiple></input>
                        )}
                            
                        </Form.Item>
                        
                        
                    </Form>
                </Modal>
              </div> 
            </div>
             <Footer></Footer>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Color = Form.create({ name: 'normal_login' })(Color));
