import React, { Component } from 'react';
import Dashboard from './dashboard/dashboard';
import Users from './users/users';
import Category from './category/category';
import MainCategory from './main category/main category';
import Country from './country/country';
//import Usage from './usage/usage';
import About from './about/about';
import Terms from './terms/terms';
import Privacy from './privacy/privacy';

import Report from './report/report';
import Login from './login/login';
import Admin from './admin/admin'
import Ads from './ads/ads'
import Brands from './brand/brand'
import Model from './model/model'
import BrandInfo from './brand info/brand info'
import ModelInfo from './model info/model info'
import Color from './color/color'
import ColorInfo from './color info/color info'
import  AnoncementInfo from './anoncement info/anoncement info'

import Anoncement from './anoncement/anoncement'
import Contactus from './contactus/contactus'
import AdminInfo from './admin info/admin info'
import UserInfo from './user info/user info'
import Splash from './splash/splash'
import CategoryInfo from './category info/category info'
import CountryInfo from './country info/country info'
import AdsInfo from './ads info/ads info'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import {store,persistor } from '../store';
import { PersistGate } from 'redux-persist/integration/react'
import Conditions from './conditions/conditions'
import CityInfo from './city info/city info'
import Trends from './trends/trends'
import Slider from './slider/slider'
import SliderInfo from './slider info/slider info'
import Problem from './problem/problem'
import ProblemInfo from './problem info/problem info'
import Suggestion from './suggestion/suggestion'
import SuggestionInfo from './suggestion info/suggestion info'
import TopAds from './top ads/top ads'
class App extends Component {
  componentDidMount(){
   // askForPermissioToReceiveNotifications()
  }
  //
  render() {
    return (
      <Provider store={store}>
         <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <div className="App">
          <Switch>
            <Route path='/TopAds' component={TopAds}/>
            <Route path='/Dashboard' component={Dashboard}/>
            <Route  path='/Login' component={Login}/>
            <Route  path='/Slider' component={Slider}/>
            <Route  path='/SliderInfo' component={SliderInfo}/>
            <Route  path='/CityInfo' component={CityInfo}/>
            <Route  path='/Trends' component={Trends}/>
            <Route  path='/Conditions' component={Conditions}/>
            <Route path='/Contactus' component={Contactus}/>
            <Route path='/AnoncementInfo' component={AnoncementInfo}/>
            <Route path='/AdsInfo' component={AdsInfo}/>
            <Route exact path='/' component={Splash}/>
            <Route path='/users' component={Users} />
            <Route path='/terms' component={Terms} />
            <Route path='/Admins' component={Admin} />
            <Route path='/category' component={Category} />
            <Route path='/Main category' component={MainCategory} />
            <Route path='/reports' component={Report} />
            <Route path='/AdminInfo' component={AdminInfo} />
            <Route path='/UserInfo' component={UserInfo} />
            <Route path='/CategoryInfo' component={CategoryInfo} />
            <Route path='/announcement' component={Anoncement} />
            <Route path='/countries' component={Country} />
            <Route path='/CountryInfo' component={CountryInfo} />
            <Route path='/Ads' component={Ads} />
            <Route path='/about' component={About} />
            {/*<Route path='/usage' component={Usage} /> */}
            <Route path='/privacy' component={Privacy} />
            <Route path='/brand' component={Brands} />
            <Route path='/model' component={Model} />
            <Route path='/brandInfo' component={BrandInfo} />
            <Route path='/modelInfo' component={ModelInfo} />
            <Route path='/color' component={Color} />
            <Route path='/colorInfo' component={ColorInfo} />
            <Route path='/problems' component={Problem} />
            <Route path='/ProblemInfo' component={ProblemInfo} />
            <Route path='/suggestions' component={Suggestion} />
            <Route path='/SuggestionInfo' component={SuggestionInfo} />

          </Switch>
        </div>
      </BrowserRouter>
      </PersistGate>
      </Provider>
    );
  }
}

export default App;
