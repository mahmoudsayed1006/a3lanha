import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './category info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {  message,Modal, Form, Input,Icon} from 'antd';
import {Table} from 'react-materialize'

class CategoryInfo extends React.Component {
    state = {
        modal1Visible: false,
         category:this.props.location.state.data,
         flag:this.props.location.state.flag,
         file: this.props.location.state.data.img,
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
         onChange = (e) => {
            this.setState({file:e.target.files[0]});
        }
        
         componentDidMount()
         {
             //console.log("ANWEr")
             //console.log(this.props.currentUser)
            
             //this.props.location.state.data
             //console.log(this.props.location.state.data)
         }

    addSerVice = (service) => {
      let uri = '';
      if(service){
        uri = `${BASE_END_POINT}categories/${this.state.category.id}/service`
      }else{
        uri = `${BASE_END_POINT}categories/${this.state.category.id}/unservice`
      }
      let l = message.loading(allStrings.wait, 2.5)
      axios.put(uri,null,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
      .then(response=>{
        l.then(() => message.success(allStrings.done, 2.5))
        this.props.history.goBack()
    })
    .catch(error=>{
       console.log( 'service error    ',  error.response)
        l.then(() => message.error('Error', 2.5))
    })

    }     
    
    deleteCategory = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}categories/${this.state.category.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }

     trenCategory = () => {
      let l = message.loading(allStrings.wait, 2.5)
      axios.put(`${BASE_END_POINT}categories/${this.state.category.id}/trend`,{},{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
      .then(response=>{
          l.then(() => message.success(allStrings.categoryBecomeTrend, 2.5))
          this.props.history.goBack()

      })
      .catch(error=>{
          console.log(error.response)
          l.then(() => message.error('Error', 2.5))
      })
   }

   unTrenCategory = () => {
    let l = message.loading(allStrings.wait, 2.5)
    axios.put(`${BASE_END_POINT}categories/${this.state.category.id}/untrend`,{},{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.token}`
      },
    })
    .then(response=>{
        l.then(() => message.success(allStrings.categoryBecomeUntrend, 2.5))
        this.props.history.goBack()

    })
    .catch(error=>{
        console.log(error.response)
        l.then(() => message.error('Error', 2.5))
    })
 }

     deleteSubCategory = (id) => {
      let l = message.loading(allStrings.wait, 2.5)
      axios.delete(`${BASE_END_POINT}categories/${id}`,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
      .then(response=>{
        console.log(response)
          l.then(() => message.success(allStrings.deleteDone, 2.5))
          this.props.history.push('/Main category')

      })
      .catch(error=>{
          console.log(error.response)
          l.then(() => message.error('Error', 2.5))
      })
   }

     handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          //console.log('Received values of form: ', values);
          var form = new FormData();
          if(this.state.file){
            form.append('img', this.state.file);
          }
          if(values.priority){
            form.append('priority',values.priority)
          }
         
          form.append('name', values.categoryname);
          form.append('arabicName', values.arabicname);
    
          let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}categories/${this.state.category.id}`,form,{
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }

      
      //modal
     
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const {category} = this.state

        const {select} = this.props;
      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff',marginTop:"10px"}}>{allStrings.categoryInfo}</h2>
                </div>
                <div class="row">
                <Carousel images={category.img} />
                </div>
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.name}>
                            </input>
                            <label for="name" class="active">{allStrings.EnglishCategoryName}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.arabicName}>
                            </input>
                            <label for="name" class="active">{allStrings.ArabicCategoryName}</label>
                            </div>

                        </div>
                        {category.main&&
                          <div class="row">
                              <div class="input-field col s6">
                              <input id="name" type="text" class="validate" disabled value={category.priority}>
                              </input>
                              <label for="name" class="active">{allStrings.priority}</label>
                              </div>
                          </div>
                        }


      

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.id}>
                            </input>
                            <label for="name" class="active">{allStrings.id}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.main}>
                            </input>
                            <label for="name" class="active">{allStrings.mainCategories}</label>
                            </div>
                        </div>
                        {!category.main&&
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.parent}>
                            </input>
                            <label for="name" class="active">{allStrings.parent}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.trend}>
                            </input>
                            <label for="name" class="active">{allStrings.trend}</label>
                            </div>

                        </div>
                        }
                       {category.main&&
                        <div className='dash-table'>
                            <h5>{allStrings.subCategory} : </h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="title">{allStrings.EnglishCategoryName}</th>
                                            <th data-field="price">{allStrings.ArabicCategoryName}</th>
                                            
                                            <th>{allStrings.remove}</th>
                                            </tr>
                                        </thead>  

                                        <tbody>
                                        {category.child.map(val=>(
                                            <tr>
                                                <td  onClick={()=>{this.props.history.push('/Category2Info',{data:val}) }}>{val.id}</td>
                                                <td  onClick={()=>{this.props.history.push('/Category2Info',{data:val}) }}>{val.name}</td>                                 
                                                <td  onClick={()=>{this.props.history.push('/Category2Info',{data:val}) }}>{val.arabicName}</td>
                                                
                                                <td
                                                 onClick={()=>{this.deleteSubCategory(val.id)}}
                                                >
                                                  <Icon className='controller-icon' type="delete" style={{ fontSize: "16px"}}/>
                                                </td>
                                            </tr>
                                        ))}

                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div> 
                       }

                        
                       
                            <a class="waves-effect waves-light btn btn-large delete " onClick={this.deleteCategory}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>
                            <a class="waves-effect waves-light btn btn-large edit" onClick={this.trenCategory}>{allStrings.trend}</a>
                            <a class="waves-effect waves-light btn btn-large edit" onClick={this.unTrenCategory}>{allStrings.untrend}</a>



                            <div>
                            <Modal
                                title={allStrings.edit}
                                visible={this.state.modal1Visible}
                                onOk={this.handleSubmit}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal1Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmit} className="login-form">

                               <label for="name" class="lab">{allStrings.EnglishCategoryName}</label>
                                  <Form.Item>
                                  {getFieldDecorator('categoryname', {
                                      rules: [{ required: true, message: 'Please enter category name by english' }],
                                      initialValue: category.name
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>

                                  <label for="name" class="lab">{allStrings.ArabicCategoryName}</label>
                                  <Form.Item>
                                  {getFieldDecorator('arabicname', {
                                      rules: [{ required: true, message: 'Please enter category name by arabic' }],
                                      initialValue: category.arabicName
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>
                                  {category.main&&
                                  <div>
                                 <label for="name" class="lab">{allStrings.priority}</label>

                                   <Form.Item>
                                   {getFieldDecorator('priority', {
                                       rules: [{ required: true, message: 'Please enter category order' }],
                                       initialValue: category.priority
                                   })(
                                       <Input  />
                                   )}
                                   </Form.Item>
                                   </div>
                                  }
                           
                                
                                 
                              </Form>
                              <input class='file' type="file" onChange= {this.onChange} multiple></input>
                            </Modal>

                                
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(CategoryInfo = Form.create({ name: 'normal_login', })(CategoryInfo)) ;
