import React from 'react';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './model.css';
import { Skeleton, message,Modal, Form, Icon, Input, Button,Popconfirm,Select} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class Model extends React.Component {
  page = 0;
  pagentationPage=0;
  counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedModel:null,
     Models:[],
     parents:[],
     file:null,
     loading:true,
     tablePage:0,
     }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

     onChange = (e) => {
      this.setState({file:e.target.files[0]});
     
  }
    //submit form
    flag = 0;
    getModels = (page,deleteRow) => {
      axios.get(`${BASE_END_POINT}model?page=${page}&limit=20`)
      .then(response=>{
        console.log("ALL Models    ",response.data.data)
        console.log(response.data)
        if(deleteRow){
          this.setState({tablePage:0})
         }
         this.setState({Models:deleteRow?response.data.data:[...this.state.Models,...response.data.data],loading:false})
        })
      .catch(error=>{
        console.log("ALL brands ERROR")
        console.log(error.response)
        this.setState({loading:false})
      })
    }
    
    componentDidMount(){
      this.getModels(1,true)
    }
    OKBUTTON = (e) => {
      this.deleteModel()
     }
     getParents = (page) => {
      axios.get(`${BASE_END_POINT}brand?page=${page}&limit={20}`)
      .then(response=>{
      //  console.log("ALL Categories")
      //  console.log(response.data)
        this.setState({parents:response.data.data})
      })
      .catch(error=>{
     //   console.log("ALL Categories ERROR")
        console.log(error.response)
      })
    }
     deleteModel = () => {
       let l = message.loading(allStrings.wait, 2.5)
       axios.delete(`${BASE_END_POINT}model/${this.state.selectedModel}`,{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
       .then(response=>{
           l.then(() => message.success(allStrings.deleteDone, 2.5))
           this.getModels(1,true)
           this.flag = 0
       })
       .catch(error=>{
          // console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            //console.log('Received values of form: ', values);
            var form = new FormData();
            form.append('img',this.state.file);
            form.append('modelname', values.Modelname);
            form.append('brand', values.brand.key);
            let l = message.loading(allStrings.wait, 2.5)
            axios.post(`${BASE_END_POINT}model`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.addDone, 2.5));
                this.setState({ modal1Visible:false,file:null });
                this.fileInput.value = "";
                this.getModels(1,true)
                this.flag = 0;
                this.props.form.resetFields()
            })
            .catch(error=>{
                console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })
          }
        });
        
      }
     
    //end submit form

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
         const Option = Select.Option;
          let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
         let list =this.state.Models.map((val,index)=>[
          val.id,val.modelname,val.brand.brandname,
          controls
        ])
         

          const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
           const {select} = this.props;
      return (
          <div>
              <AppMenu height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
              <Tables
               columns={this.state.loading?['loading...']: [allStrings.id,allStrings.modelname,allStrings.brandname, allStrings.remove]} 
              title={allStrings.ModelTable}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!==3){
                  
                  //console.log(this.state.countries[cellMeta.rowIndex])
                  this.props.history.push('/modelInfo',{data:this.state.countries[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===3){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedModel:id})
                    //console.log(id)
                  }
              }}
              onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{ 
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                console.log('current',currentPage)
              
                if(currentPage%2!=0  && currentPage > this.flag){
                  let rem = currentPage %2;
                  let x = currentPage - rem;
                  console.log(rem)
                  console.log(x)
                  this.flag  = this.flag +1;
                  console.log("flage",this.flag)
                    let page = rem + this.flag;
                    console.log("page",page)
                    this.getModels(page)
                    console.log("request",page)
                    console.log("flag",this.flag) 

                } 
               

                  
              }}
              arr={this.state.loading?loadingView:list}></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addModel}</Button>
              <Modal
                    title={allStrings.addModel}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                        {getFieldDecorator('Modelname', {
                            rules: [{ required: true, message: 'Please enter Model name' }],
                        })(
                            <Input placeholder={allStrings.Modelname}/>
                        )}
                        </Form.Item>

                        <Form.Item>
                        {getFieldDecorator('brand', {
                            rules: [{ required: true, message: 'Please enter brand name' }],
                        })(
                            <Select labelInValue  
                            placeholder={allStrings.brandname}
                            style={{ width: '100%'}} >
                            {this.state.parents.map(
                                val=><Option value={val.id}>{val.brandname}</Option>
                            )}
                            </Select>
                        )}
                        </Form.Item>
                        
                    </Form>
                </Modal>
              </div> 
            </div>
             <Footer></Footer>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Model = Form.create({ name: 'normal_login' })(Model));
