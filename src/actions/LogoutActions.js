import { LOGOUT,LOGOUT_LOADING,LOGOUT_LOADING_END } from "./types";
import { BASE_END_POINT } from '../config/URL';

import axios from 'axios';


export  function logout(FB_token, BE_token, history) {
    //console.log('dddddddddddddqqqq')
    return  dispatch => {
        dispatch({type:LOGOUT_LOADING})
        axios.post(`${BASE_END_POINT}logout`, JSON.stringify({
            token: FB_token 
          }), {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${BE_token}`
          },
        }).then(response => {
            console.log('log out')
            console.log(response);
            
            localStorage.removeItem('@QsathaUser')
            history.push('/Login')       
            
            dispatch({ type: LOGOUT })
          
        }).catch(error => {
            console.log('log out error')
           console.log(error)
            console.log(error.response)
           
        });
        
    }
}