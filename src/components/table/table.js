import React from 'react';
import MUIDataTable from "mui-datatables";
import './table.css';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
class Tables extends React.Component {
    getMuiTheme = () => createMuiTheme({
      overrides: {
        typography: {
          useNextVariants: true,
        },
        MuiPaper:{
          root:{
            margin:"50px",
            paddingRight: "0px !important"
          }
        },
        MUIDataTableToolbar: {
          root: {
            backgroundColor: "#edeff2",
            borderTopLeftRadius: "5px",
            borderTopRightRadius: "5px",
            

          },
          actions:{
            flex:" 0 0 45%",
            textAlign: "left"
          },
          left:{
            flex: ".4 0 30%"
          }
        },
        MuiTypography: {
          h6: {
            color:"#000 !important",
            fontWeight: 600,
           fontFamily: 'Times',
           fontSize:25,
          }
        },
        MUIDataTableHeadCell:{
          root:{
            fontSize:"14px",            
          },
          data:{
            paddingLeft: "17px",
            paddingTop: "15px !important",
            paddingBottom: "15px !important"
          }
        },
        MuiTableCell:{
          root:{
            padding:"0px !important"           
          },
         
        },
        MuiPopover:{
          paper:{
            top:'130px !important',
            //left:'1000px !important'
          }
          
          
      }
       
      }
    })
    render() {
      const {columns,onCellClick,onChangePage,page} =this.props 
      const {title} =this.props
      const {arr} =this.props
     
      const data = [
        ...arr
      ];
 
  
      const options = {
        page:page,
        search:false,
        filter:false,
        //filterType: "dropdown",
        responsive: "scroll",
        selectableRows: false,
       // onRowClick: onRowClick,
        onCellClick: onCellClick ,
        onChangePage: onChangePage,
        sort: false,
      };
  //(rowData,rowMeta)=>console.log(rowData)
      return (
          <div className='myTable'>
          <MuiThemeProvider theme={this.getMuiTheme()}>
              <MUIDataTable
                title={title}
                data={data}
                columns={columns}
                options={options}
                
                />
          </MuiThemeProvider>
          </div>
        
      );
    }
  }

export default Tables;
