import React, { Component } from 'react';
import './menu.css';
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
//import {Button, SideNav ,SideNavItem,Icon,Collapsible,CollapsibleItem} from 'react-materialize'
import { Menu, Icon} from 'antd';
import "antd/dist/antd.css";
import {SelectMenuItem } from '../../actions/MenuAction'

const { SubMenu } = Menu;

class AppMenu extends Component {
  constructor(props){
    super(props)
    if(this.props.isRTL){
      allStrings.setLanguage('ar')
    }else{
      allStrings.setLanguage('en')
    }
  }

  state = {
    collapsed: false,
  }

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
     const {goTo,height} = this.props;
     console.log("GO TO   ",goTo)
    const x  = !this.props.select;
    return (
      <div style={{direction:"rtl"}} >
    
         <Menu  
         style={{position:'fixed'}}       
          //defaultSelectedKeys={[this.props.key]}
          //defaultOpenKeys={['sub1']}
          mode="inline"
          theme="dark"
          inlineCollapsed={this.props.select}
        >
          <Menu.Item  style={{marginTop: "14px"}}
              onClick={()=>{
               
              this.props.SelectMenuItem(0,x)
              }} >
              <Icon type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'} />
              
          </Menu.Item>

          <Menu.Item
            style={{}}
            onClick={()=>{
              goTo.push('/Dashboard')
              //alert("")
              console.log("GO TO   ",goTo)
              this.props.SelectMenuItem(1,true)
            }}
            key="1" style={{marginBottom:6}}>
            <Icon type="home"   style={{fontSize:20}} />
              <span  >{allStrings.home}</span>
          </Menu.Item>       

          <SubMenu
            key="sub1"
            title={
              <span>
                <Icon  type="usergroup-add" style={{fontSize:20}}/>
                <span>{allStrings.users}</span>
              </span>
            }
          >
              <Menu.Item
              onClick={()=>{
                //alert("")
                goTo.push('/users')
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem(2,true)
              }}
              key="2" style={{marginBottom:6}}>
              <Icon type="usergroup-add" style={{fontSize:20}} />
                <span  >{allStrings.users}</span>
              </Menu.Item>
              <Menu.Item
              onClick={()=>{
                //alert("")
                goTo.push('/Admins')
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem(3,true)
              }}
              key="3" style={{marginBottom:6}} >
                <Icon type="user-delete" style={{fontSize:20}} />
                <span>{allStrings.admin}</span>
              </Menu.Item>

          </SubMenu>
         
          <SubMenu
            key="sub2"
            title={
              <span>
                <Icon type="appstore" style={{fontSize:20}}/>
                <span>{allStrings.categories}</span>
              </span>
            }
          >
             <Menu.Item
              onClick={()=>{
                goTo.push('/category')
                //alert("")
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem('7',true)
              }}
              key="70" style={{marginBottom:6}}>        
              <Icon type="appstore"   style={{fontSize:20}} />
                <span  >{allStrings.subCategory}</span>
              </Menu.Item>

              <Menu.Item
              onClick={()=>{
                goTo.push('/Main category')
                //alert("")
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem('10',true)
              }}
              key="72" style={{marginBottom:6}}>        
              <Icon type="book"   style={{fontSize:20}} />
                <span  >{allStrings.mainCategories}</span>
              </Menu.Item>
              <Menu.Item
              onClick={()=>{
                goTo.push('/Trends')
                //alert("")
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem('20',true)
              }}
              key="75" style={{marginBottom:6}}>  
                    
              <Icon type="rise"   style={{fontSize:20}} />
                <span  >{allStrings.trending}</span>
            </Menu.Item>

          </SubMenu>


        <Menu.Item
          onClick={()=>{
            goTo.push('/Ads')
            //alert("")
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem('20',true)
          }}
          key="74" style={{marginBottom:6}}>        
          <Icon type="picture"   style={{fontSize:20}} />
            <span  >{allStrings.ads}</span>
        </Menu.Item>

        <Menu.Item
          onClick={()=>{
            goTo.push('/TopAds')
            //alert("")
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem('262',true)
          }}
          key="74" style={{marginBottom:6}}>        
          <Icon type="picture"   style={{fontSize:20}} />
            <span  >{allStrings.topAds}</span>
        </Menu.Item>


        <Menu.Item
          onClick={()=>{
            goTo.push('/announcement')
            //alert("")
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem('20',true)
          }}
          key="33" style={{marginBottom:6}}> 
                 
          <Icon type="desktop"   style={{fontSize:20}} />
            <span  >{allStrings.announcement}</span>
        </Menu.Item>
     

        <Menu.Item
            onClick={()=>{
              goTo.push('/countries')
              //alert("")
              console.log("GO TO   ",goTo)
              this.props.SelectMenuItem('21',true)
            }}
            key="44" style={{marginBottom:6}}>        
            <Icon type="flag"    style={{fontSize:20}} />
              <span  >{allStrings.countries}</span>
          </Menu.Item>



      
       
        <SubMenu
            key="sub12"
            title={
              <span >
                <Icon  type="container" />
                <span>{allStrings.terms}</span>
              </span>
            }
          >
          <Menu.Item
        onClick={()=>{
          goTo.push('/terms')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('20',true)
        }}
         key="11" style={{marginBottom:6}}>        
        <Icon type="container"   style={{fontSize:20}} />
          <span  >{allStrings.terms}</span>
        </Menu.Item>

        <Menu.Item
        onClick={()=>{
          goTo.push('/about')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('20',true)
        }}
         key="12" style={{marginBottom:6}}>        
        <Icon type="container"   style={{fontSize:20}} />
          <span  >{allStrings.about}</span>
        </Menu.Item>

        <Menu.Item
        onClick={()=>{
          goTo.push('/privacy')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('20',true)
        }}
         key="13" style={{marginBottom:6}}>        
        <Icon type="container"   style={{fontSize:20}} />
          <span  >{allStrings.privacy}</span>
        </Menu.Item>

        <Menu.Item
        onClick={()=>{
          goTo.push('/Conditions')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('20',true)
        }}
         key="14" style={{marginBottom:6}}>        
        <Icon type="container"   style={{fontSize:20}} />
          <span  >{allStrings.conditions}</span>
        </Menu.Item>
        
        
   
          </SubMenu>
          <SubMenu
            key="sub132"
            title={
              <span >
                <Icon type="customer-service" />
                <span>{allStrings.contactUS}</span>
              </span>
            }
          >
          <Menu.Item 
          onClick={()=>{
            //alert("")
            goTo.push('/Contactus')
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem(5,true)
          }}
          key="5"style={{marginBottom:6}}>
            <Icon type="wechat"  style={{fontSize:20}}/>
            <span>{allStrings.contactUS}</span>
          </Menu.Item>
          <Menu.Item 
           onClick={()=>{
            //alert("")
            goTo.push('/problems')
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem(634,true)
          }}
          key="6" style={{marginBottom:6}}>
            <Icon type="exclamation-circle" style={{fontSize:20}} />
            <span>{allStrings.problems}</span>
          </Menu.Item>
          <Menu.Item 
           onClick={()=>{
            //alert("")
            goTo.push('/suggestions')
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem(604,true)
          }}
          key="62" style={{marginBottom:6}}>
            <Icon type="form" style={{fontSize:20}} />
            <span>{allStrings.suggestions}</span>
          </Menu.Item>
         
          </SubMenu>

          <Menu.Item 
           onClick={()=>{
            //alert("")
            goTo.push('/reports')
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem(6,true)
          }}
          key="63" style={{marginBottom:6}}>
            
            <Icon type="ordered-list" style={{fontSize:20}} />
            <span>{allStrings.reports}</span>
          </Menu.Item>

          <Menu.Item 
           onClick={()=>{
            //alert("")
            goTo.push('/Slider')
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem(602,true)
          }}
          key="60" style={{marginBottom:6}}>
            
            <Icon type="play-circle" style={{fontSize:20}} />
            <span>{allStrings.slider}</span>
          </Menu.Item>
          <Menu.Item 
           onClick={()=>{
            //alert("")
            goTo.push('/brand')
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem(602,true)
          }}
          key="61" style={{marginBottom:6}}>
            <Icon type="car" style={{fontSize:20}} />
            <span>{allStrings.brand}</span>
          </Menu.Item>
          <Menu.Item 
           onClick={()=>{
            //alert("")
            goTo.push('/color')
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem(602,true)
          }}
          key="95" style={{marginBottom:6}}>
            <Icon type="highlight" style={{fontSize:20}} />
            <span>{allStrings.colors}</span>
          </Menu.Item>

          
        </Menu>

      </div>

    );
  }
}
const mapToStateProps = state => ({
  isRTL: state.lang.isRTL,
  currentUser: state.auth.currentUser,
  key: state.menu.key,
  select: state.menu.select
})

const mapDispatchToProps = {
  SelectMenuItem,
}

export default connect(mapToStateProps,mapDispatchToProps) (AppMenu);
