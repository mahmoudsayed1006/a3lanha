
import React, { Component } from 'react';
import './nav.css';
import {Icon} from 'react-materialize';
import { List, Avatar,Dropdown,Menu,Button,Form,Modal} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import { connect } from 'react-redux';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import {logout} from '../../actions/LogoutActions'
import  {allStrings} from '../../assets/strings'
import {ChangeLanguage} from '../../actions/LanguageAction'




class Nav extends Component {
   page =1;
   MSGpages =1;
   peoplePages=1;

    state = {
      selectedImage:null,
      seen:false,
      currentFreindName:'',
      currentFriend:0,
      notifs:[],
      count:[],
      unseenCount:[],
      modal1Visible: false,
      modal2Visible: false,
      once:false,
    }

    constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

    componentDidMount(){
      this.getNotif()
      this.getCount()

      setTimeout(()=>{
        this.page=0
        this.getNotif(true)
       this.getCount()
      },1000*60*3)


       
    }

    readNotification = (notID) => {
      //console.log(notID)
    axios.put(`${BASE_END_POINT}notif/${notID}/read`,{}, {
        headers: {
          'Content-Type': 'application/json',
          //this.props.currentUser.token
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
    }).then(Response=>{
        console.log("notification is red")
        console.log(Response)
        this.page=1;
        //this.setState({notifs:[]})
        //this.getNotif();
        this.getCount()
        
        //this.props.getUnreadNotificationsNumers(this.props.currentUser.token)
    }).catch(error=>{
        //console.log(error.response)
    })
}

    getNotif= (reload) => {
      this.page += 1
      axios.get(`${BASE_END_POINT}notif?page=${this.page-1}&limit=20`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
      .then(response=>{
        console.log("ALL notif   ",response.data)
        //console.log(response.data)
        this.setState({
          notifs: 
         reload? response.data.data : [...this.state.notifs , ...response.data.data ]   
          })
          this.page += 1
     
      })
      .catch(error=>{
        //console.log("ALL notif ERROR")
        //console.log(error.response)
      })
    }


    getCount= () => {
      axios.get(`${BASE_END_POINT}notif/unreadCount`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
      .then(response=>{
        //console.log("ALL notif")
        //console.log(response.data.data)
        this.setState({
          count: response.data
          })
        
      })
      .catch(error=>{
        //console.log("ALL notif ERROR")
        //console.log(error.response)
      })
    }
   
    
    setModal1Visible(modal1Visible) {
      this.setState({ modal1Visible });
    }
    setModal2Visible(modal2Visible) {
      this.setState({ modal2Visible });
    }



  render() {
    const { getFieldDecorator } = this.props.form;
    const {logout, currentUser, userToken,history} = this.props;
    //console.log(this.props.isRTL)

        const menu2 = (
          <Menu>
            <Menu.Item
            onClick={()=> this.props.history.push('/AdminInfo',{data:this.props.currentUser.user})}
             key="0">{allStrings.profile}</Menu.Item>
            <Menu.Divider />
             <Menu.Item 
            onClick={()=>{
              this.props.ChangeLanguage(true)
              allStrings.setLanguage('ar')
              localStorage.setItem('@lang','ar'); 
            }}
             key="2">{allStrings.arabic}</Menu.Item>
             <Menu.Item 
            onClick={()=>{
              this.props.ChangeLanguage(false)
              allStrings.setLanguage('en')
              localStorage.setItem('@lang','en');
            }}
             key="3">{allStrings.english}</Menu.Item>
              <Menu.Item 
            onClick={()=>logout(userToken, currentUser.token, history)}
             key="1">{allStrings.logout}</Menu.Item>
          </Menu>
        );
        const data = this.state.notifs.map(val=>[""+val.id,""+val.resource.firstname,""+val.resource.img,""+val.description,""+val.read])
      //console.log(data)
        const menu = (
        <List  onScroll={() => /*this.page <= 3 &&*/ this.getNotif()}
          itemLayout="horizontal"
          dataSource={this.state.notifs}
          renderItem={item => (
            <List.Item 
            onClick={()=>{
             
              if(item.read === false){
                this.readNotification(item.id)
              }
              
              /*if(item.description.toLowerCase().includes("new order")){
                this.props.history.push('/pendingOrder',{data:item})
              }*/
              this.props.history.push('/AdsInfo',{data:item.ads})

            }} >
              <List.Item.Meta style={{padding: "10px" , backgroundColor:!item.read&&'#e1e1e1'}} 
                avatar={<Avatar src={item.resource.img} />}
                title={<a href="#">{item.resource.username}</a>}
                description={item.description}

              />
            </List.Item>
            
          )}
          /*footer={this.page>=3 && <Button onClick={() => this.getNotif()}>loading more</Button>}*/
          
        />
      );


    
    return (
      <nav>
        
      
        <div className={`nav-wrapper ${!this.props.select&&'nav-wrapper-active'}`}>
          <ul className="right hide-on-med-and-down">
            
            
            <li className='action'>
            <Dropdown overlay={menu} trigger={['click']}>
              <a className="ant-dropdown-link" href="#">
             <Icon>notifications</Icon><span className="badge red">{this.state.count.unread}</span>
              </a>
            </Dropdown>
            </li>
            {/*<li className='action'>
            <Dropdown overlay={this.menuMessage()} trigger={['click']}>
              <a className="ant-dropdown-link" href="#">
              <Icon>chat_bubble</Icon><span class="badge red">{this.state.unseenCount.unseen}</span>
              </a>
            </Dropdown>
            </li>
            */}
            <li className='action'>
            <Dropdown overlay={menu2} trigger={['click']}>
              <a className="ant-dropdown-link" href="#">
              <Icon>more_vert</Icon>
              </a>
            </Dropdown>
            
            </li>

          

            
            
          </ul>
        </div>
      </nav> 
    );
  }
}

const mapToStateProps = state => ({
  isRTL: state.lang.isRTL,
  currentUser: state.auth.currentUser,
  userToken: state.auth.userToken,
  select: state.menu.select
})

const mapDispatchToProps = {
  logout,
  ChangeLanguage
}

export default withRouter(connect(mapToStateProps,mapDispatchToProps)(Nav = Form.create({ name: 'normal_login' })(Nav)));

